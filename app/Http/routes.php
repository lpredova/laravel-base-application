<?php


Route::get('/', ['as' => 'home', 'uses' => 'Website\PagesController@homepage']);
Route::get('/login', ['as' => 'login', 'uses' => 'Website\PagesController@login']);
Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController',]);


Route::group(['prefix' => 'admin'], function () {

    Route::get('/', ['as' => 'dashboard', 'uses' => 'Admin\DashboardController@index']);
    Route::get('/users', ['as' => 'users', 'uses' => 'Admin\UsersController@index']);

});