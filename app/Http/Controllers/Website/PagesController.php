<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Requests;

class PagesController extends Controller
{
    public function homepage()
    {
        return view('public.pages.homepage');
    }

    public function login()
    {
        return view('auth.login');
    }
}
