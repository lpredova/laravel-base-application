<!DOCTYPE HTML>
<html>
@include('public.components.head')
<body>
@include('public.components.navigation')

@yield('content')

@include('public.components.footer')
@include('public.components.scripts')
</body>
</html>