<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Hackaton example</title>

    <!-- CSS  -->
    {!! HTML::style('https://fonts.googleapis.com/icon?family=Material+Icons') !!}
    {!! HTML::style('/css/style.css') !!}
    {!! HTML::style('/css/materialize.css') !!}

</head>