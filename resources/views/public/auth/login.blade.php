{{--@extends('public.template')

@section('content')
    <div class="login-page">
        <div class="container">
            <div style="margin-bottom:0" class="row">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(array('url' => url('/auth/login'),'class'=>'col s12 m8 l6 card-panel center-align login-form offset-l3')) !!}
                <h2 class="truncate">Login</h2>

                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" name="email" class="validate"/>
                        <label for="email">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" type="password" name="password" class="validate"/>
                        <label for="password">Password</label>
                    </div>
                </div>
                <button type="submit" class="waves-effect waves-light btn">Login</button>
                <small>
                    <h6><a href="/register" class="account">Don't have an account?</a></h6></small>
                {!! Form::close()!!}
            </div>
        </div>
    </div>
@endsection--}}
