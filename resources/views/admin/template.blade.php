<!DOCTYPE HTML>
<html>
@include('admin.components.head')
<body>

<div class="wrapper">
    @include('admin.components.sidePanel')
    <div class="main-panel">
        @include('admin.components.navigation')
        <div class="content">
            @yield('content')
        </div>

        @include('admin.components.footer')
    </div>
</div>
@include('admin.components.scripts')
</body>
</html>
